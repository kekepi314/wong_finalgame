﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControllerScript : MonoBehaviour
{
    public GameObject player;
    public bool gameOver;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
        if (Input.GetKey(KeyCode.R) && gameOver)
        {
            SceneManager.LoadScene("FirstLevel");
        }
    }

    public void GameOver()
    {
        gameOver = true;
        Time.timeScale = 0f;
    }
}
