﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPlatformerController : PhysicsObject
{

    public float maxSpeed = 20;
    public float jumpTakeOffSpeed = 12;
    public Text healthText;
    public Text winText;
    public Text scoreText;
    public int health;
    public int score;
    public GameControllerScript gcScript;

    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private int jumpCount;
    private float dashTimer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        health = 2;
        score = 0;
        SetHealthText();
        SetScoreText();
        winText.text = "";
        jumpCount = 2;
        dashTimer = 0f;
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        if (dashTimer <= 0f)
        {
            move.x = Input.GetAxis("Horizontal") / 3;
        }
        if (grounded)
        {
            jumpCount = 2;
        }
        if (Input.GetButtonDown("Jump") && jumpCount > 0)
        {
            velocity.y = jumpTakeOffSpeed;
            jumpCount -= 1;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
                velocity.y = velocity.y * 0.6f;
        }

        bool flipSprite = spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f);
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        dashTimer -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.LeftShift) && dashTimer < -1f)
        {
            dashTimer = 0.25f;
        }
        else if (dashTimer > 0)
        {
            velocity.y = 0;
            move.x = spriteRenderer.flipX ? -0.75f : 0.75f;
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;

        if (transform.position.y < -10 && !gcScript.gameOver)
        {
            health -= 1;
            SetHealthText();

            if (health > 0)
            {
                transform.position = Vector3.zero;
            }
            
        }

    }

    public void SetHealthText()
    {
        healthText.text = "Health: " + health.ToString();
        if (health <= 0)
        {
            winText.text = "You Lose!";
            spriteRenderer.enabled = false;
            gcScript.GameOver();
        }
    }
    public void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
        if (score >= 6)
        {
            winText.text = "You Win!";
            gcScript.GameOver();
        }
    }
}
