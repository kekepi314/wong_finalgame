﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParryScript : MonoBehaviour
{
    public Vector3 mouseWorldPos;
    public GameObject player;

    private float currentTime;
    private float endTime = 1f;
    private PolygonCollider2D swordCollider;
    private SpriteRenderer swordRenderer;

    void Start()
    {
        currentTime = 0f;
        swordRenderer = GetComponent<SpriteRenderer>();
        swordCollider = GetComponent<PolygonCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bool down = Input.GetMouseButtonDown(0);

        if (down)
        {
            swordRenderer.enabled = true;
            swordCollider.enabled = true;
            currentTime = 0f;
        }

        currentTime += Time.deltaTime;
        if (currentTime >= endTime)
        {
            swordRenderer.enabled = false;
            swordCollider.enabled = false;
        }

        transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 0.5f, player.transform.position.z);

        mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y, Mathf.Rad2Deg * Mathf.Atan2(mouseWorldPos.y - transform.position.y, mouseWorldPos.x - transform.position.x) - 90));
    }
}
