﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingScript : MonoBehaviour
{

    public GameObject bullet;

    private float fireRate;
    private float nextFire;
    private SpriteRenderer spriteRenderer;
    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        fireRate = 2f;
        nextFire = Time.time;

        spriteRenderer = GetComponent<SpriteRenderer>();
        target = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(target.transform.position, transform.position) < 10f)
        {
            CheckIfTimeToFire();
        }

        bool flipSprite = (spriteRenderer.flipX ? (target.transform.position.x - transform.position.x > 0.01f) : (target.transform.position.x - transform.position.x < -0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    void CheckIfTimeToFire()
    {
        if (Time.time > nextFire)
        {
            Instantiate(bullet, transform.position, Quaternion.identity);
            nextFire = Time.time + fireRate;
        }
    }
}

// Code from Alexander Zotov: https://www.youtube.com/watch?v=kOzhE3_P2Mk